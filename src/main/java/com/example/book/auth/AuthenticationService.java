package com.example.book.auth;

import com.example.book.email.EmailService;
import com.example.book.email.EmailTemplateName;
import com.example.book.role.Role;
import com.example.book.role.RoleRepository;
import com.example.book.secutiy.JwtService;
import com.example.book.user.Token;
import com.example.book.user.TokenRepository;
import com.example.book.user.User;
import com.example.book.user.UserRepository;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthenticationService {


    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenRepository tokenRepository;
    private final EmailService emailService;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    @Value("${application.mailing.frontend.activation-url}")
    private String activationUrl;


    public void register(RegistrationRequest registrationRequest) throws MessagingException {

        Role role = roleRepository.findByName("USER").orElseThrow(() -> new IllegalStateException("Role USER is not initialized"));

        User user = User.builder()
                .firstname(registrationRequest.getFirstname())
                .lastname(registrationRequest.getLastname())
                .email(registrationRequest.getEmail())
                .password(passwordEncoder.encode(registrationRequest.getPassword()))
                .roles(List.of(role))
                .accountLocked(false)
                .enabled(false)
                .build();
        userRepository.save(user);
        sendValidationEmail(user);
    }

    private void sendValidationEmail(User user) throws MessagingException {
        String newToken = generateAndSaveActivationToken(user);
        emailService.sendEmail(
                user.getEmail(),
                user.getFullname(),
                "Account activation",
                EmailTemplateName.ACTIVATE_ACCOUNT,
                activationUrl,
                newToken
        );

    }

    private String generateAndSaveActivationToken(User user) {
        String generatedCode = generateActivationCode(6);
        Token token = Token.builder()
                .token(generatedCode)
                .createdAt(LocalDateTime.now())
                .expiredAt(LocalDateTime.now().plusMinutes(5))
                .user(user)
                .build();
        tokenRepository.save(token);
        return generatedCode;
    }

    private String generateActivationCode(int length) {
        String charecters = "0123456789";
        StringBuilder codeBuilder = new StringBuilder();
        SecureRandom secureRandom = new SecureRandom();
        for (int i = 0; i < length; i++) {
            int randomIndex = secureRandom.nextInt(charecters.length());
            codeBuilder.append(charecters.charAt(randomIndex));
        }
        return codeBuilder.toString();

    }

    public AuthenticateResponse authenticate(AuthenticateRequest authenticateRequest) {
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticateRequest.getEmail(),
                        authenticateRequest.getPassword()
                )
        );
        User user = (User) auth.getPrincipal();
        var claims = new HashMap<String, Object>();
        claims.put("fullname", user.getFullname());
        String token = jwtService.generateToken(claims, user);
        return AuthenticateResponse.builder()
                .token(token)
                .build();
    }

   // @Transactional
    public void activateAccount(String token) throws MessagingException {
        Token savedToken= tokenRepository.findByToken(token).orElseThrow(()-> new RuntimeException("token is not exixts"));
        if(LocalDateTime.now().isAfter(savedToken.getExpiredAt())){
            sendValidationEmail(savedToken.getUser());
            throw new RuntimeException("Token was expired. A new Token has been sent");
        }
        User user = userRepository.findById(savedToken.getUser().getId()).orElseThrow(()->new RuntimeException("User is not exixt"));
        user.setEnabled(true);
        userRepository.save(user);
        savedToken.setValidatedAt(LocalDateTime.now());
        tokenRepository.save(savedToken);
    }
}
